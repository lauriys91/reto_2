package com.reto2;

public class AvionCompetencia extends Avion {
    
    // Constantes
    public static final int TIEMPO = 2;
    
    // Constructores

    // Constructores 1
    public AvionCompetencia(double peso, double tamanio){
        super(peso, tamanio);
    }
    
    // Constructores 2
    public AvionCompetencia(double precioBase){
        super(precioBase);
    }
    
    // Constructores 3
    public AvionCompetencia(){
    }
    
    // Metodos
    public double calcularPrecio(){
        return super.getPrecioBase() + (super.getPeso()*super.getTamanio()*TIEMPO);
    }
    
}