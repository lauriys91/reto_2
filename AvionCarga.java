package com.reto2;

// CLASE AVIÓN CARGA

public class AvionCarga extends Avion {

    // Constantes
    public static final double CAPACIDAD = 8.0;

    //Atributo Local
    public double calcularPrecioAvionCarga;
    public double precioFinal;

    // Constructores

    // Constructores 1
    public AvionCarga(double peso, double tamanio){
        super(peso, tamanio);
    }

    // Constructores 2
    public AvionCarga(double precioBase){
        super(precioBase);
    }

    // Constructores 3
    public AvionCarga(){
    }
    
    // Metodos
    public double calcularPrecio(){
        return super.getPrecioBase() + (super.getPeso()*super.getTamanio()*CAPACIDAD);
    }
    
}

