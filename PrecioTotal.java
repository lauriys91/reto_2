package com.reto2;

// Inicio de la solución

//CLASE PRECIO TOTAL
public class PrecioTotal {

    // Atributos
    public double totalPrecios;
    public double totalAvionCarga;
    public double totalAvionCompetencia;
    public Avion[] avion;

    // Constructores
    public PrecioTotal(Avion[] avion){
        this.avion = avion;
    }
    
    // Metodos
    public void calcularTotales() {

        for (int i = 0; i <= avion.length - 1; i++){
            totalPrecios += avion[i].calcularPrecio();

            if (avion[i] instanceof AvionCarga){
                totalAvionCarga += avion[i].calcularPrecio();
            } else {
                totalAvionCompetencia += avion[i].calcularPrecio();
            }
        }
    }
    public void mostrarTotales() {    
    // Calculo de totales
        calcularTotales();
        System.out.println("Total Avión " + totalPrecios);
        System.out.println("Total Avión Carga " + totalAvionCarga);
        System.out.println("Total Avión Competencia " + totalAvionCompetencia);
    }
}