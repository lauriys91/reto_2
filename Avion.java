package com.reto2;

// CLASE AVIÓN
public class Avion{

    // Constantes
    public static final double PESO = 10.0;
    public static final double TAMANIO = 4.5;
    public static final double PRECIO_BASE = 1000.0;
    
     // Atributos
    public double peso;
    public double tamanio;
    public double precioBase;

    // CONSTRUCTORES

    // Constructores 1
    public Avion(double peso, double tamanio){
        this.peso = peso;
        this.tamanio = tamanio;
        this.precioBase = PRECIO_BASE;
    }

    // Constructores 2
    public Avion(double precioBase){
        this.peso = PESO;
        this.tamanio = TAMANIO;
        this.precioBase = precioBase;
    }

    // Constructores 3
    public Avion(){
        this.peso = PESO;
        this.tamanio = TAMANIO;
        this.precioBase = PRECIO_BASE;
    }

    // METODOS

    public double getPeso() {
        return peso;
    }

    public double getTamanio() {
        return tamanio;
    }

    public double getPrecioBase() {
        return precioBase;
    }

    public double calcularPrecio(){
        return 0.0;
    }
}